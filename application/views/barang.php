<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>

    <!-- <link rel="stylesheet" href="<?= base_url() .'assets/dist/css/bootstrap.min.css'?>">
    <script src="<?= base_url(). 'assets/dist/js/bootstrap.min.js'?> "></script>
    <script src=" <?= base_url(). 'assets/dist/js/jquery.min.js' ?> "></script> -->

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>

</head>
<body>

    <div class="container">

        <div class="row">
            <center><h1 class="text-primary">CRUD - Codeigniter, Bootstrap, Ajax</h1></center>
        </div>

        <div class="row">
            <div class="col-md-4">
                <button class="btn btn-primary" type="button" data-toggle="modal" data-target="#modalTambah" >Tambah</button>
            </div>

            <div class="col-md-8">

            </div>
        </div>

        <div class="row">
            <table class="table table-bordered table-hover">
                <thead>
                    <tr>
                        <th>Kode Barang</th>
                        <th>Nama Barang</th>
                        <th>Harga</th>
                        <th>Stock</th>
                        <th>Satuan</th>
                        <th>Aksi</th>
                    </tr>
                </thead>

                <tbody id="show_data">
    
                </tbody>
            </table>
        </div>

        <div class="modal-tambah">
              <!-- Modal tambah start -->
        <div id="modalTambah" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h4 class="modal-title">Tambah Barang</h4>
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                </div>
                                                <div class="modal-body p-4">
                                                    <form action="" method="post" >
                                                    <div class="row">
                                                        
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label for="field-2" class="control-label">Kode</label>
                                                                <input type="text" class="form-control" id="kode_brg" name="kode_brg" required>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label for="field-3" class="control-label">Nama Barang</label>
                                                                <input type="text" class="form-control" id="nama_brg" name="nama_brg" required >
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label for="field-4" class="control-label">Harga </label>
                                                                <input type="number" class="form-control" id="harga" name="harga" >
                                                            </div>
                                                        </div>

                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label for="field-4" class="control-label">Stok </label>
                                                                <input type="number" class="form-control" id="stok" name="stok" >
                                                            </div>
                                                        </div>
                                                       
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label for="field-6" class="control-label">Satuan</label>
                                                                <input type="text" class="form-control" id="satuan" name="satuan" >
                                                            </div>
                                                        </div>
                                                    </div>

                                                    
                                                <div class="modal-footer">
                                                    <button type="submit" id="btnSimpan" class="btn btn-info waves-effect waves-light">Simpan</button>
                                                </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>  <!-- /.modal tambah -->
        </div>


        <div class="modal-edit">
            <!-- Modal edit start -->
            <div id="modalEdit" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h4 class="modal-title">Edit Barang</h4>
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                </div>
                                                <div class="modal-body p-4">
                                                    <form action="" method="post" >
                                                    <div class="row">
                                                        
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label for="field-2" class="control-label">Kode</label>
                                                                <input type="text" class="form-control" id="e_kode_brg" name="e_kode_brg" required readonly>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label for="field-3" class="control-label">Nama Barang</label>
                                                                <input type="text" class="form-control" id="e_nama_brg" name="e_nama_brg" required >
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label for="field-4" class="control-label">Harga </label>
                                                                <input type="number" class="form-control" id="e_harga" name="e_harga" >
                                                            </div>
                                                        </div>

                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label for="field-4" class="control-label">Stok </label>
                                                                <input type="number" class="form-control" id="e_stok" name="e_stok" >
                                                            </div>
                                                        </div>
                                                       
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label for="field-6" class="control-label">Satuan</label>
                                                                <input type="text" class="form-control" id="e_satuan" name="e_satuan" >
                                                            </div>
                                                        </div>
                                                    </div>

                                                    
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Close</button>
                                                    <button type="submit" id="btnEdit" class="btn btn-info waves-effect waves-light">Edit</button>
                                                </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>  <!-- /.modal Edit -->
        </div>

        <div class="modal-hapus">
            <!-- Modal edit start -->
            <div id="modalHapus" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h4 class="modal-title">Hapus Barang</h4>
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                </div>
                                                <div class="modal-body p-4">
                                                    <form action="" method="post" >
                                                    <div class="row">
                                                        
                                                        <div class="col-md-12">
                                                            <div class="text-danger"> <h3>Apakah Anda yakin akan menghapus data ini ?</h3></div>
                                                            <div class="form-group">
                                    
                                                                <input type="hidden" class="form-control" id="h_kode_brg" name="h_kode_brg" required readonly>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>

                                                    
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Cancel</button>
                                                    <button type="submit" id="btnHapus" class="btn btn-info waves-effect waves-light">Ok</button>
                                                </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>  <!-- /.modal Edit -->
        </div>


    </div>

    <script>
        $(document).ready(function(){

            load_data();

            function load_data() {

                const url = "<?php echo site_url('barang/get_all'); ?>"
                let html ='';

                console.log(url);
                
                $.ajax({
                    url: url,
                    method: "GET",
                    dataType: "JSON",
                    success: function(data){

                        console.log(data)
                        $.each(data, function(){
                            html += `<tr>
                                <td> ${this.kode_brg} </td>
                                <td> ${this.nama_brg} </td>
                                <td> ${this.harga} </td>
                                <td> ${this.stock} </td>
                                <td> ${this.satuan} </td>
                                <td> <button class="btn btn-primary item_edit" data="${this.kode_brg}">edit</button>  <button class="btn btn-danger item_hapus" data="${this.kode_brg}">hapus</button> </td>
                            </tr>`;
                        });

                        $('#show_data').html(html);
                    }
                })
            };

            // get by id untuk edit data
            $('#show_data').on('click','.item_edit',function(){    

                var id=$(this).attr('data');
                const url = "<?= site_url('barang/get_byId') ?>";

                console.log(url)

                $.ajax({
                    method: "GET",
                    url  : url,
                    dataType : "JSON",
                    data : {id:id},
                    success: function(data){
                            $.each(data,function(){
                                
                                $('#modalEdit').modal('show');
                                $('[name="e_kode_brg"]').val(this.kode_brg);
                                $('[name="e_nama_brg"]').val(this.nama_brg);
                                $('[name="e_harga"]').val(this.harga);
                                $('[name="e_stok"]').val(this.stock);
                                $('[name="e_satuan"]').val(this.satuan);
                                
                            });

                        
                    }
                });
                return false;

            });

             // get by id untuk hapus data
             $('#show_data').on('click','.item_hapus',function(){    

                var id=$(this).attr('data');
                const url = "<?= site_url('barang/get_byId') ?>";

                console.log(url)

                $.ajax({
                    method: "GET",
                    url  : url,
                    dataType : "JSON",
                    data : {id:id},
                    success: function(data){
                            $.each(data,function(){
                                
                                $('#modalHapus').modal('show');
                                $('[name="h_kode_brg"]').val(this.kode_brg);
                                
                            });

                        
                    }
                });
                return false;

            });


            // UNTUK SIMPAN DATA
            $('#btnSimpan').on('click', function(){

                const url = " <?= site_url('barang/add') ?> "
                
                let kode_brg = $("#kode_brg").val();
                let nama_brg = $("#nama_brg").val();
                let harga = $("#harga").val();
                let stok = $("#stok").val();
                let satuan = $("#satuan").val();

                $.ajax({
                    method: "POST",
                    dataType: "JSON",
                    url: url,
                    data: {kode_brg: kode_brg, nama_brg:nama_brg, harga:harga, stok: stok, satuan: satuan},
                    success: function(){

                        load_data();
                    }
                })
            }) ; 


            // Untuk edit data
            $("#btnEdit").on('click', function(){

                const url = " <?= site_url('barang/edit') ?> "

              let kode_brg = $('[name="e_kode_brg"]').val();
              let nama_brg = $('[name="e_nama_brg"]').val();
              let harga = $('[name="e_harga"]').val();
              let stok = $('[name="e_stok"]').val();
              let satuan = $('[name="e_satuan"]').val();

               $.ajax({
                    method: "POST",
                    dataType: "JSON",
                    url: url,
                    data: {kode_brg: kode_brg, nama_brg:nama_brg, harga:harga, stok: stok, satuan: satuan},
                    success: function(){

                        load_data();
                    }
                })

            });


            // method hapus data
            $("#btnHapus").on('click', function(){
                
                const url = " <?= site_url('barang/delete') ?> "

                let kode_brg = $('[name="h_kode_brg"]').val();

                $.ajax({
                    method: "POST",
                    dataType: "JSON",
                    url: url,
                    data: {kode_brg: kode_brg},
                    success: function(){

                        load_data();
                    }
                })

            })

        });
    </script>
</body>
</html>