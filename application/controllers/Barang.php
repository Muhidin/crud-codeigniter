<?php 
    header("Access-Control-Allow-Origin: *");
    header("Access-Control-Allow-Methods: GET,POST,OPTIONS,DELETE,PUT");
     header("Access-Control-Request-Headers: Content-Type, X-Requested-With, Authorization");
defined('BASEPATH') OR exit('No direct script access allowed');

class Barang extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('model_barang');
    }

    public function index()
    {
        $this->load->view('barang');
        
    }

    public function add()
    {
        $kode_brg = $this->input->post('kode_brg');
        $nama_brg = $this->input->post('nama_brg');
        $harga = $this->input->post('harga');
        $stock = $this->input->post('stok');
        $satuan = $this->input->post('satuan');

        $data = $this->model_barang->add($kode_brg,$nama_brg,$harga,$stock,$satuan);

        echo json_encode($data);
    }

    public function edit()
    {
        $kode_brg = $this->input->post('kode_brg');
        $nama_brg = $this->input->post('nama_brg');
        $harga = $this->input->post('harga');
        $stock = $this->input->post('stok');
        $satuan = $this->input->post('satuan');

        $data = $this->model_barang->edit($kode_brg,$nama_brg,$harga,$stock,$satuan);

        echo json_encode($data);
    }

    public function delete()
    {
        $kode_brg = $this->input->post('kode_brg');

        $data = $this->model_barang->delete($kode_brg);

        echo json_encode($data);
    }

    public function get_all()
    {
        $data = $this->model_barang->get_all()->result();

        echo json_encode($data);
    }

    public function get_byId()
    {
        $kode_brg = $this->input->get('id');

        $data = $this->model_barang->get_byId($kode_brg)->result();

        echo json_encode($data);
    }

}

/* End of file Controllername.php */
