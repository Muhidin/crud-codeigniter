<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_barang extends CI_Model {

    public function add($kode_brg,$nama_brg,$harga,$stock,$satuan)
    {
        $data = array(
            "kode_brg" => $kode_brg,
            "nama_brg" => $nama_brg,
            "harga" => $harga,
            "stock" => $stock,
            "satuan" => $satuan
        );

        return $this->db->insert('barang', $data);
        
    }

    public function edit($kode_brg,$nama_brg,$harga,$stock,$satuan)
    {
        $data = array(
            "nama_brg" => $nama_brg,
            "harga" => $harga,
            "stock" => $stock,
            "satuan" => $satuan
        );

        $this->db->where('kode_brg', $kode_brg);
        return $this->db->update('barang', $data);
    }

    public function delete($kode_brg)
    {
        $this->db->where('kode_brg', $kode_brg);
        return $this->db->delete('barang');
        
    }

    public function get_byId($kode_brg)
    {
        return $this->db->get_where('barang', array('kode_brg' => $kode_brg));
    }

    public function get_all()
    {
        return $this->db->get('barang');
    }

}

/* End of file ModelName.php */
